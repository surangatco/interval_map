#include <assert.h>
#include <map>
#include <limits>


template<class K, class V>
class interval_map {
    friend void IntervalMapTest();

private:
    std::map<K,V> m_map;

public:
    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map( V const& val) {
        m_map.insert(m_map.begin(),std::make_pair(std::numeric_limits<K>::lowest(),val));
    }

    // Assign value val to interval [keyBegin, keyEnd). 
    // Overwrite previous values in this interval. 
    // Do not change values outside this interval.
    // Conforming to the C++ Standard Library conventions, the interval 
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval, 
    // and assign must do nothing.
    void assign( K const& keyBegin, K const& keyEnd, V const& val ) 
    {
        if (keyBegin < keyEnd)
        {
            auto itrLow = (--m_map.upper_bound(keyBegin));
            auto itrHigh = (--m_map.upper_bound(keyEnd));



            if ((itrLow->second == val) || (itrHigh->second == val))
            {
                printf("this entry is not allowed. the map  must be canonical.\n");
            }
            else
            {
                auto initialitrLow = itrLow;
                auto initialitrHigh = itrHigh;

                auto valHigh = itrHigh->second;
                auto itrHigherHigh = ++itrHigh;

                if (initialitrLow != initialitrHigh)
                    m_map.erase(++itrLow, itrHigherHigh);

                if (initialitrLow->first < keyBegin)
                {
                    m_map.emplace_hint(itrHigherHigh, keyBegin, val);

                    m_map.emplace_hint(itrHigherHigh, keyEnd, valHigh);

                }
                else
                {
                    //means itrLow->first == keyBegin
                    initialitrLow->second = val;

                    m_map.emplace_hint(itrHigherHigh, keyEnd, valHigh);

                }

            }
            
        }
        else
        {
            printf("empty range. do nothing.\n");
        }
    }

    //void test(K const& key, K const& key2, V const& val){}

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const {
        return ( --m_map.upper_bound(key) )->second;
    }

    //print the map for testing purpose
    void print_map()
    {
        for (auto itr = m_map.begin(); itr != m_map.end(); itr++)
        {
            printf("%d -> %c\n", itr->first, itr->second);
        }
    }
};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a function IntervalMapTest() here that tests the
// functionality of the interval_map, for example using a map of unsigned int
// intervals to char.

void IntervalMapTest()
{
    //initial map
    //(std::numeric_limits<K>::lowest(),A)
    interval_map<unsigned int, char> map1('A');

    map1.print_map();



    //test1 - insert (3,7,D)
    printf("\ntest1\n");
    map1.assign(3,7,'D');
    map1.print_map();


    //test2 - insert (5,11,B)
    printf("\ntest2\n");
    map1.assign(5,11,'B');
    map1.print_map();

    //test3 - insert (3,5,C)
    printf("\ntest3\n");
    map1.assign(3,5,'C');
    map1.print_map();


    //test4 - insert (3,3,E)
    printf("\ntest4\n");
    map1.assign(3,3,'E');
    map1.print_map();

    //test5 - insert (6,8,B)
    printf("\ntest5\n");
    map1.assign(6,8,'B');
    map1.print_map();


}

int main() {
    IntervalMapTest();
}
